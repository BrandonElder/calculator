// main events

function btnValue(){
  var input   = document.querySelector("#calc-screen-input"),
      buttons = document.querySelectorAll("button.calc-button");
  for (i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function(event) {
      input.value = input.value + event.currentTarget.value
    })
  }
}

function inputVal(){
  var appendBtn = document.querySelector("button.enter-btn");
  appendBtn.addEventListener("click", function(event) {
    var input     = document.getElementById("calc-screen-input").value,
        screen    = document.getElementById('calc-screen-top');
        wrap      = document.getElementById('top-screen-wrap');
        node      = document.createElement("div"),
        textnode  = document.createTextNode(input);
    node.appendChild(textnode);
    screen.appendChild(node);
    var node2 = node.cloneNode(true);
    screen.appendChild(node2);
    wrap.scrollTop = wrap.scrollHeight - wrap.clientHeight;
    document.getElementById("calc-screen-input").value = " ";
  });
}

function backspace() {
  var backBtn = document.querySelector("#backspace-btn");
  backBtn.addEventListener("click", function() {
    var input = document.getElementById("calc-screen-input").value;
    document.getElementById("calc-screen-input").value = input.substr(0, input.length - 1);
  });
}

function menuBtnActive(){
  $(".calc-menu-btn").click(function(){
    var $this   = $(this),
        clicked = $this.data('clicked');
    if(clicked){
      $(this).css('color', 'black');
    } else {
      $(this).css('color', '#3ac1a6');
    }
    $this.data('clicked', !clicked);
  });
}

// This seems  pretty hacky and needs some work... Couldn't figure it out in pure Javascript.
function switchKeypad(){
  $(".calc-menu-btn").click(function(){
    var containerId = $(this).attr("id"),
    content = $("div.content." + containerId);
    $("div.content:not('." + containerId + "')").hide();
    content.show(); 
    $("#calculator-button-container").toggleClass('main-btns');
  });
}

$(".calc-menu-btn").mouseup(function(){
  clearTimeout(pressTimer);
  // Clear timeout
  return false;
}).mousedown(function(){
  // Set timeout
  pressTimer = window.setTimeout(function() {
    $(this).css('background', 'red');
  },1000);
  return false; 
});

document.addEventListener('DOMContentLoaded', function(){ 
  btnValue();
  inputVal();
  backspace();
  menuBtnActive();
  switchKeypad();
}, false);

